(function () {
	'use strict';

	// load local .env early
	var dotenv = require('dotenv');
	dotenv.load();

	var _ = require('lodash'),
		gulp = require('gulp'),
		spawn = require('child_process').spawn,node,
		jshint = require('gulp-jshint'),
		concat = require('gulp-concat'),
		rename = require('gulp-rename'),
		uglify = require('gulp-uglify'),
		jasmine = require('gulp-jasmine'),
		fs = require('fs');

	// models
	require(__dirname + '/models/base.js');

	var codePath = [
		'./*.js',
		'./controllers/*.js',
		'./models/*.js',
		'./routes/controllers/*.js',
	];
	var testPath = './tests/*.tests.js';

	// Lint JS
	gulp.task('lint', function() {
			return gulp.src(codePath)
					.pipe(jshint())
					.pipe(jshint.reporter('default'));
	});

	//Run unit tests
	gulp.task('tests', function(){
			return gulp.src(testPath)
					.pipe(jasmine());
	});

	// Concat & Minify JS
	gulp.task('minify', function(){
			return gulp.src(codePath)
					.pipe(concat('all.js'))
					.pipe(gulp.dest('dist'))
					.pipe(rename('all.min.js'))
					.pipe(uglify())
					.pipe(gulp.dest('dist'));
	});

	// Watch Our Files
	gulp.task('watch', function() {
			gulp.watch(codePath, ['lint', 'tests', 'run']);
	});

	gulp.task('run', function(){
			if (node)
				node.kill();
			node = spawn('node', ['--debug','server.js'], {stdio: 'inherit'});
			node.on('close', function (code) {
					if (code === 8) {
							console.log('Error detected, waiting for changes...');
					}
			});
	});

	gulp.task('db_drop', function(cb){
		db.sequelize.drop().then(function(){
			cb();
		},
		function(err) {
			cb(err);
		});
	});

	// sync database models
	gulp.task('db_sync', ['db_public_schema_reset'], function(cb){
		db.sequelize.sync({force:true}).then(function(){
			cb();
		},
		function(err) {
			cb(err);
		});
	});

	// drop/create public schema
	gulp.task('db_public_schema_reset', function(cb){
		db.sequelize.query('DROP SCHEMA public CASCADE; CREATE SCHEMA public;').then(function(){
			cb();
		},
		function(err) {
			cb(err);
		});
	});

	//run in development
	gulp.task('develop', ['default','run', 'watch']);

	//runs lint, minify, and watch tasks..
	gulp.task('default', ['lint', 'minify']);

} ()); // use strict
