(function () {
  'use strict';

  //i'm using .env to load env variables
  require('dotenv').load();

  var restify = require('restify'),
    restify_routes = require('restify-routes'),
    _ = require('lodash');

  // models into global
  require(__dirname + '/models/base.js');

  var server = restify.createServer();
  server.use(restify.CORS());
  server.use(restify.queryParser());
  server.use(restify.acceptParser(server.acceptable));
  server.use(restify.fullResponse());
  server.use(restify.bodyParser());
  server.pre(authorize);
  server.on('MethodNotAllowed', unknownMethodHander);

  // import routes
  restify_routes.set(server, __dirname + '/routes');

  server.listen(process.env.PORT, function () {
    console.log('%s listening at %s', server.name, server.url);
  });

  //inform user of options..
  function unknownMethodHander(req, res) {
    if('options' === req.method.toLowerCase()) {

      var allow_headers = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Origin', 'X-Requested-With', 'Authorization'];

      if(0 >= res.methods.indexOf('OPTIONS')) {
        res.methods.push('OPTIONS');
      }
      res.header('Access-Control-Allow-Credentials', false);
      res.header('Access-Control-Allow-Headers', allow_headers.join(', '));
      res.header('Access-Control-Allow-Methods', res.methods.join(', '));
      res.header('Access-Control-Allow-Origin', req.headers.origin);

      return res.send(200);

    } else {
      return res.send(new restify.MethodNotAllowedError());
    }
  }

  function authorize(req, res, next) {

    var authorize_header = req.header('Authorization', null);
    if(!authorize_header) {
      return next();
    }
    else
    {
      //TODO: code to process user authentication
      return next();
    }
  }

} ()); // use strict
