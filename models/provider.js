(function() {
  'use strict';

  module.exports = function(sequelize, DataTypes) {

    return sequelize.define('provider', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(256),
        allowNull: false,
        unique: false
      },
      cert_level: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      },
    });
  };
}()); // use strict
