(function() {
  'use strict';

  module.exports = function(sequelize, DataTypes) {

    return sequelize.define('patient', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(254),
        allowNull: false,
        unique: false
      },
      age: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      }
    });
  };
}()); // use strict
