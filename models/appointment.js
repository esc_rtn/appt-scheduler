(function() {
  'use strict';
  var moment = require('moment');
  require('moment-range');

  module.exports = function(sequelize, DataTypes) {

    return sequelize.define('appointment', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      reason: {
        type: DataTypes.STRING(254),
        allowNull: false,
        unique: false
      },
      date: {
        type: DataTypes.DATE,
        allowNull: false,
        unique: false
      },
      duration: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      }
    }, {
      instanceMethods: {
        end_time: function() {
          return (moment.tz(this.date, process.env.TIMEZONE)).add(5,
            'm').add(this.duration, 'm');
        },
        start_time: function() {
          return moment.tz(this.date, process.env.TIMEZONE);
        },
        conflicts_with: function(provider) {

          for (var a in provider.appointments) {
            var prov_range = moment.range(provider.appointments[a].start_time(),
              provider.appointments[a].end_time());
            var appt_range = moment.range(this.start_time(), this.end_time());
            if (prov_range.overlaps(appt_range)) {
              return true;
            }
          }
          return false;
        },
        is_after_hours: function() {
          if (this.start_time().isBetween(this.start_time().startOf(
              'day').add(9, 'h'), this.start_time().startOf('day').add(
              16, 'h')) &&
            this.end_time().isBetween(this.end_time().startOf('day').add(
              9, 'h'), this.end_time().startOf('day').add(16, 'h'))) {
            return false;
          } else {
            return true;
          }
        }
      }
    });
  };
}()); // use strict
