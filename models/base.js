(function() {
  'use strict';

  // global singleton db/orm
  if (!global.hasOwnProperty('db')) {

    var Sequelize = require('sequelize'),
      sequelize = null,
      SequelizeImport = require('sequelize-import'),
      async = require('async');

    // setup development env config
    sequelize = new Sequelize(process.env.DATABASE, process.env.USERNAME,
      process.env.PASSWORD, {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        protocol: process.env.DB_PROTOCOL,
        dialect: process.env.DB_DIALECT,
        define: {
          underscored: true
        },
        pool: {
          maxConnections: 5,
          maxIdleTime: 30
        },
      });

    global.db = {
      Sequelize: Sequelize,
      sequelize: sequelize,
    };

    //setup models in sequelize
    async.series([
      function import_models(async_cb) {
        global.db.models = new SequelizeImport(__dirname, sequelize, {
          exclude: ['base.js']
        });
        async_cb(null);
      },
      function define_associations(async_cb) {
        var models = global.db.models; // alias
        models.service.hasMany(models.appointment);
        models.patient.hasMany(models.appointment);
        models.provider.hasMany(models.appointment);

        async_cb(null);
      }
    ]);
  }
  module.exports = global.db;

}()); // use strict
