(function() {
  'use strict';

  module.exports = function(sequelize, DataTypes) {

    return sequelize.define('service', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(254),
        allowNull: false,
        unique: true
      },
      req_cert: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      },
      req_age: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      },
      duration: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: false
      },
    });
  };
}()); // use strict
