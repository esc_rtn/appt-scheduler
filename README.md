# README #
RESTful appt scheduling api written in Node.js using Restify, postgres w/Sequelize, Gulp.js, and Moment.js

### Running API ###

* Install Postgres and create an empty db named 'appt-scheduler'
* Pull code
* Create .env file with configuration (see below)
* Install node, run npm install
* run: gulp db_sync
* run: gulp develop

### .env settings ###

ENV=development  
DATABASE = 'appt-scheduler'  
DB_USER = 'your-postgres-user-name'  
DB_PASSWORD = 'your-postgres-password'  
DB_HOST = 'localhost'  
DB_PORT = '5432'  
DB_PROTOCOL = 'tcp'  
DB_DIALECT = 'postgres'  
PORT = '8080'  
TIMEZONE = 'America/Chicago'