(function() {
  'use strict';

  var restify = require('restify');
  var moment = require('moment');

  module.exports = {
    //create appointment
    '/api/appointment': {
      post: function(req, res, next) {
        var service, provider, patient;

        //build non-persistant instance of appt
        var appointment = db.models.appointment.build({
          patient_id: req.params.appointment.patient_id,
          provider_id: req.params.appointment.provider_id,
          service_id: req.params.appointment.service_id,
          reason: req.params.appointment.reason,
          duration: req.params.appointment.duration,
          date: req.params.appointment.date
        });

        if (appointment.is_after_hours()) {
          return next(new restify.BadRequestError(
            "Appointment cannot exceed office hours. (9a - 4p CST). Requested: " +
            appointment.start_time().format(
              "dddd, MMMM Do YYYY, h:mm:ss a")));
        }

        // fetch service, provider, and patient requested
        if (req.params.appointment.service_id) {
          service = db.models.service.find({
            where: {
              id: req.params.appointment.service_id
            },
            include: {
              model: db.models.appointment
            }
          });
        } else {
          return next(new restify.BadRequestError(
            "service_id must be supplied."));
        }

        if (req.params.appointment.provider_id) {
          provider = db.models.provider.find({
            where: {
              id: req.params.appointment.provider_id
            },
            include: {
              model: db.models.appointment
            }
          });
        } else {
          return next(new restify.BadRequestError(
            "provider_id must be supplied."));
        }

        if (req.params.appointment.patient_id) {
          patient = db.models.patient.find({
            where: {
              id: req.params.appointment.patient_id
            },
            include: {
              model: db.models.appointment
            }
          });
        } else {
          return next(new restify.BadRequestError(
            "patient_id must be supplied."));
        }

        //ensure appointment meets business criteria

        service.then(function(service) {
            if (!service) {
              return next(new restify.BadRequestError(
                "Service specified cannot be found"));
            }
            if (service.duration > req.params.appointment.duration) {
              return next(new restify.BadRequestError(
                "Service duration exceeds appointment duration."));
            }
            //check provider
            provider.then(function(provider) {
                if (!provider) {
                  return next(new restify.BadRequestError(
                    "Provider specified cannot be found"));
                }

                if (provider.cert_level < service.req_cert) {
                  return next(new restify.BadRequestError(
                    "Provider can not perform this service due to their certificate level"
                  ));
                }

                if (appointment.conflicts_with(provider)) {
                  return next(new restify.BadRequestError(
                    "Provider has an appointment already scheduled for this time."
                  ));
                }

                //check patient
                patient.then(function(patient) {
                    if (!patient) {
                      return next(new restify.BadRequestError(
                        "Patient specified cannot be found"));
                    }
                    if (patient.age < service.req_age) {
                      return next(new restify.BadRequestError(
                        "Patient does not meet age requirements for this service."
                      ));
                    }

                    //everything's ok.. save
                    appointment.save()
                      .then(function(appointment) {
                        res.json({
                          'appointment': appointment
                        });
                        return next();
                      })

                    .catch(function(err) {
                        return next(new restify.InternalError(err));
                      },
                      function(err) {
                        console.log(err);
                      });
                  })
                  .catch(function(err) {
                      return next(new restify.InternalError(err));
                    },
                    function(err) {
                      console.log(err);
                    });
              })
              .catch(function(err) {
                  return next(new restify.InternalError(err));
                },
                function(err) {
                  console.log(err);
                });
          })
          .catch(function(err) {
              return next(new restify.InternalError(err));
            },
            function(err) {
              console.log(err);
            });

      },
    },
    '/api/appointments': {
      get: function(req, res, next) {
        db.models.appointment.findAll()

        .then(function(appointments) {

          res.json({
            'appointments': appointments
          });
          return next();
        })

        .catch(function(err) {
            return next(new restify.InternalError(err));
          },
          function(err) {
            console.log(err);
          });
      },
    },

  };
}()); // use strict
