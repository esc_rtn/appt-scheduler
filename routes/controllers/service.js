(function() {
  'use strict';

  var restify = require('restify');

  module.exports = {

    '/api/service': {
      post: function(req, res, next) {

        db.models.service.create({
            name: req.params.service.name,
            req_cert: req.params.service.req_cert,
            req_age: req.params.service.req_age,
            duration: req.params.service.duration,
          })
          .then(function(service) {
            res.json({
              'service': service
            });
            next();
          });

      },
    },
  };
}()); // use strict
