(function() {
  'use strict';

  var restify = require('restify');

  module.exports = {

    '/api/patient': {
      post: function(req, res, next) {

        db.models.patient.create({
            name: req.params.patient.name,
            age: req.params.patient.age,
          })
          .then(function(patient) {
            res.json({
              'patient': patient
            });
            next();
          });

      },
    },
  };
}()); // use strict
