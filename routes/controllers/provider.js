(function() {
  'use strict';

  var restify = require('restify');

  module.exports = {

    '/api/provider': {
      post: function(req, res, next) {

        db.models.provider.create({
            name: req.params.provider.name,
            cert_level: req.params.provider.cert_level,
          })
          .then(function(provider) {
            res.json({
              'provider': provider
            });
            next();
          });

      },
    },
  };
}()); // use strict
